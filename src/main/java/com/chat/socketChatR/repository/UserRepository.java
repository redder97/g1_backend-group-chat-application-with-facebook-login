package com.chat.socketChatR.repository;

import com.chat.socketChatR.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> findBySocialUserId(String socialUserId);
    public Optional<User> findByEmail(String email);
}
