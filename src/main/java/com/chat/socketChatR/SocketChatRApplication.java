package com.chat.socketChatR;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;

@SpringBootApplication
public class SocketChatRApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocketChatRApplication.class, args);
	}



}
