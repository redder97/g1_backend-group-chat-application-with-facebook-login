package com.chat.socketChatR.service;

import com.chat.socketChatR.domain.User;
import com.chat.socketChatR.domain.UserStatus;
import com.chat.socketChatR.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Optional<User> getUserByFacebookId(String facebookId){
        return userRepository.findBySocialUserId(facebookId);
    }

    public Optional<User> getExistingUser(User user){
        return userRepository.findByEmail(user.getEmail());
    }

    public User newUser(User user){
        user.updateUserStatus(UserStatus.NEW_USER);
        return userRepository.save(user);
    }

    public User newUserCreateHandler(User user){
        User existingUser = userRepository.findBySocialUserId(user.getSocialUserId())
                .orElseThrow(() -> new RuntimeException("User does not Exist!"));

        existingUser.updateHandler(user.getHandler());
        existingUser.updateUserStatus(UserStatus.EXISTING_USER);

        return userRepository.save(existingUser);
    }



}
