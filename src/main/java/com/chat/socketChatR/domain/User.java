package com.chat.socketChatR.domain;

import javax.persistence.*;

@Entity
public class User extends AuditableEntity{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    @Column(name="id", insertable=true, updatable=true, unique=true, nullable=false)
    private Long id;

    private String socialUserId;

    private String name;
    private String email;
    private String handler;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    public User(Long id, String socialUserId, String handler, String name, String email, UserStatus userStatus) {
        this.id = id;
        this.socialUserId = socialUserId;
        this.handler = handler;
        this.name = name;
        this.email = email;
        this.userStatus = userStatus;
    }

    /*
     *Required by JPA. Do not use.
     * */
    protected User(){}

    public void updateUserStatus(UserStatus userStatus){
        this.userStatus = userStatus;
    }

    public void updateHandler(String handler){ this.handler = handler; }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public Long getId() {
        return id;
    }

    public String getSocialUserId() {
        return socialUserId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getHandler() { return handler; }

    public boolean isExistingUser(){
        return this.userStatus.equals(UserStatus.EXISTING_USER);
    }
}
