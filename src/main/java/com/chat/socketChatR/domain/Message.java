package com.chat.socketChatR.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Message extends AuditableEntity{

    @Id
    @GeneratedValue
    private Long id;

    private String message;
    private String sender;
    private String recipient;

    public Message(String message, String sender, String recipient) {
        this.message = message;
        this.sender = sender;
        this.recipient = recipient;
    }

    /*
    * For Jpa. Do not use.
    * */
    protected Message(){}

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }


}
