package com.chat.socketChatR.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadLoginCredentialsException extends RuntimeException {

    public BadLoginCredentialsException(String message) {
        super(message);
    }
}
