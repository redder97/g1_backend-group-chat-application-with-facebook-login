package com.chat.socketChatR.controller;

import com.chat.socketChatR.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import static org.springframework.data.domain.Sort.Direction.DESC;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/socket/messages")
@CrossOrigin("*")
public class MessageController {

    @Autowired
    MessageRepository messageRepository;

    @GetMapping( value = "")
    public ResponseEntity<?> fetchMessages(@PageableDefault(size = 10, sort = "dateCreated", direction = DESC) Pageable pageable){
        return ResponseEntity.ok(messageRepository.findAll(pageable));
    }
}
