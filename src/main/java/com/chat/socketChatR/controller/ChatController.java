package com.chat.socketChatR.controller;

import com.chat.socketChatR.domain.Message;
import com.chat.socketChatR.repository.MessageRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/socket")
@CrossOrigin("*")
public class ChatController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private MessageRepository messageRepository;


    @PostMapping( consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> useSimpleRest(@RequestBody Optional<Message> message){
        if(message.isPresent()){
            Message incomingMessage = message.get();

            //if the toId is present the message will be sent privately else broadcast it to all users
            if(!incomingMessage.getRecipient().equals("")){
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+incomingMessage.getRecipient(),incomingMessage);
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+incomingMessage.getSender(),incomingMessage);
            }else{
                this.simpMessagingTemplate.convertAndSend("/socket-publisher",incomingMessage);
            }
            messageRepository.save(incomingMessage);
            return new ResponseEntity<>(incomingMessage, new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @MessageMapping("/send/message")
    public Map<String, String> useSocketCommunication(String message){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> messageConverted = null;
        try {
            messageConverted = mapper.readValue(message, Map.class);
        } catch (IOException e) {
            messageConverted = null;
        }
        if(messageConverted!=null){
            if(messageConverted.containsKey("toId") && messageConverted.get("toId")!=null && !messageConverted.get("toId").equals("")){
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+messageConverted.get("toId"),messageConverted);
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+messageConverted.get("fromId"),message);
            }else{
                this.simpMessagingTemplate.convertAndSend("/socket-publisher",messageConverted);
            }
        }
        return messageConverted;
    }

    @MessageMapping("/send/typing")
    public Map<String, String> isTypingListenerSocketCom(String receivedActivity){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> typingActivity = null;
        try {
            typingActivity = mapper.readValue(receivedActivity, Map.class);
        } catch (IOException e) {
            typingActivity = null;
        }

        if(typingActivity!=null){
            if(typingActivity.containsKey("user") && typingActivity.get("user") != null ){
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/typing", typingActivity);
            }
        }

        return typingActivity;
    }

}