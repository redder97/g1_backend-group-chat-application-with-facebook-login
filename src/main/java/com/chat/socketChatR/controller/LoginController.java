package com.chat.socketChatR.controller;

import com.chat.socketChatR.domain.User;
import com.chat.socketChatR.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/socket/login")
@CrossOrigin("*")
public class LoginController {

    @Autowired
    UserService userService;

    @PostMapping( value = "/user")
    public ResponseEntity<?> loginAttempt(@RequestBody Optional<User> user){
        User loginUser = user.orElseThrow(() -> new BadLoginCredentialsException("Bad Login Credentials."));

        Optional<User> existingUser = userService.getExistingUser(loginUser);

        if(existingUser.isPresent()){
            return ResponseEntity.ok(existingUser);
        }

        User newUser = userService.newUser(loginUser);
        return ResponseEntity.ok(newUser);
    }

    @PostMapping( value = "/user/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> newUser(@RequestBody Optional<User> user){
        User newUser = user.orElseThrow(() -> new RuntimeException("No User found"));


        User createdUser = userService.newUserCreateHandler(newUser);
        return ResponseEntity.ok(createdUser);
    }

    @GetMapping( value = "/logged-in/{facebookId}")
    public ResponseEntity<?> loggedInUser(@PathVariable("facebookId") String facebookId){
        User loggedInUser = userService.getUserByFacebookId(facebookId)
                .orElseThrow(() -> new RuntimeException("User Not Found."));
        return ResponseEntity.ok(loggedInUser);
    }
}
